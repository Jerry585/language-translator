import Header from "../../components/header/Header";
import HistoryList from "../../components/history/History";
import LogoutButton from "../../components/logoutButton/LogoutButton";
import UserButton from "../../components/userButton/userButton";

const UserPage = () => {
    return (
        <div>
            <Header>
                <LogoutButton />
                <UserButton />
            </Header>
            <HistoryList />
        </div>
    );
};

export default UserPage;
