import React from "react";
import "./LogoutButton.css"
import { useUser } from "../../shared/context/UserContext";
import { useNavigate } from "react-router-dom";

const LogoutButton = () => {
    const { user, setUser } = useUser();
    const navigate = useNavigate();


    const logout = () => {
        sessionStorage.removeItem(user)
        navigate('/')
    }

    return(
        <button className="logoutButton" onClick={logout}>
            Logout
        </button>
    )
}

export default LogoutButton