import './userButton.css'
import { useUser } from '../../shared/context/UserContext'
import Logo from '../../assets/images/Logo.png';
import {Link} from 'react-router-dom';

const UserButton = () => {
    const {user, setUser} = useUser()

  return (
    <div>
      <div className="userButton-container">
        <img className="logoButton" src={Logo} alt="Button-Logo" />
        <Link to="/user">
          <button className="userButton">{user}</button>
        </Link>
      </div>
    </div>
  );
};

export default UserButton;
