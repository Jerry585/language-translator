import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import './Translation.css'
import { useUser } from "../../shared/context/UserContext";
import { addTranslation } from "../../shared/api/TranslationAPI";

// import '../../assets/images/translationSign'


const Translation = () => {
    const {user} = useUser()
    const [input, setInput] = useState("")
    const [translation, setTranslation] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        let img = []
        for (let i = 0; i < input.length; i++) {
            img.push(`/${input[i]}.png`)
        }
        
        let userData = sessionStorage.getItem(user)
        const timeOut = setTimeout(() => {setTranslation(img); addTranslation(userData, input)}, 1000)
        
        if (sessionStorage.getItem(user) === null) { navigate("/")}
        
        return () => clearTimeout(timeOut)
        
    }, [input])
    

    return (
        
        <div className="translation-page-container">
            <input type="text" value={input} onChange={event => setInput(event.target.value.replace(/[^A-Za-z ]/ig, ''))} className="translation-textbox-input" placeholder="Enter a word here..." />
            <div type="text" className="translation-textbox-result">
                <div className="signResult">
                    {translation.map(sign => sign==="/ .png" ? <span className="noImage" /> : <img src={sign} alt=" " className="signs"/>)}
                </div>
            </div>
        </div>
    )
}

export default Translation